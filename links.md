# Angular Boot Camp
[Angular Boot Camp Curriculum](https://github.com/angularbootcamp/abc)

[Angular Boot Camp Zip File](http://angularbootcamp.com/abc.zip)

[Video Manager (target application)](http://videomanager.angularbootcamp.com)

[Workshop Repo](https://bitbucket.org/od-training/abc-sf20190422)

[Sample Video Data](https://api.angularbootcamp.com/videos)

[Survey](http://angularbootcamp.com/survey)

[Observable Intro](https://stackblitz.com/edit/typescript-observable-intro)

[Beginner Reactive Programming (video by Cory Rylan)](https://www.youtube.com/watch?v=-yY2ECd2tSM)

# Resources
[TypeScript Playground](http://www.typescriptlang.org/play/)

[DOM Events](https://developer.mozilla.org/en-US/docs/Web/Events)

[Flexbox Froggy](https://flexboxfroggy.com/)

[CSS Grid Garden](https://cssgridgarden.com/)

[Angular Meetup This Week](https://www.meetup.com/Angular-SF/events/259169429/)

Angular Air (Videocast and Podcast)
[link](https://angularair.com/)

Adventures in Angular (Podcast)
[link](https://devchat.tv/adv-in-angular)

ng-newsletter (weekly Angular email)
[link](https://www.ng-newsletter.com/)

Angular Top 5 (weekly Angular email)
[link](http://angulartop5.com/)

["Crying Baby" State Management talk](https://www.youtube.com/watch?v=eBLTz8QRg4Q)

# Books

[Build Reactive Websites with
RxJS](https://pragprog.com/book/rkrxjs/build-reactive-websites-with-rxjs)

# VS Code extensions
[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template)

[Angular File
Changer](https://marketplace.visualstudio.com/items?itemName=john-crowson.angular-file-changer)

[Bracket Pair Colorizer
2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

# Observable resources

[RXMarbles](http://rxmarbles.com)

[LearnRXJS](https://www.learnrxjs.io/)

[Seven Operators to Get Started with RxJS (Article)](https://www.infoq.com/articles/rxjs-get-started-operators)

["I Switched a Map" video](https://www.youtube.com/watch?v=rUZ9CjcaCEw)

[Reactive Visualizations](https://reactive.how/)

[Operator Decision Tree](https://rxjs-dev.firebaseapp.com/operator-decision-tree)

[Creating an observable from scratch (live-coding session) - Ben Lesh](https://www.youtube.com/watch?v=m40cF91F8_A)
