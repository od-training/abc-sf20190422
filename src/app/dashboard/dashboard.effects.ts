import { Injectable } from '@angular/core';
import { Actions, Effect, ROOT_EFFECTS_INIT, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';

import { VideoListArrivedAction } from '../state';
import { VideoDataService } from '../video-data.service';

@Injectable()
export class DashboardEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly svc: VideoDataService
  ) {
  }

  // ROOT_EFFECTS_INIT is a special action that is dispatched at the end of
  // NgRx's initialization process, so this effect executes at application
  // initialization.
  @Effect()
  init$ = this.actions$
    .pipe(
      ofType(ROOT_EFFECTS_INIT),
      switchMap(() => this.svc.load()),
      map(videos => new VideoListArrivedAction(videos))
    );
}
