import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { Video } from 'src/app/common/interfaces';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  video: Observable<Video>;

  constructor(ar: ActivatedRoute, svc: VideoDataService) {
    this.video = ar.queryParams.pipe(
      map(params => params.selectedVideoId),
      filter(id => !!id),
      switchMap(id => svc.getVideo(id))
    );
  }
}
