import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Video } from 'src/app/common/interfaces';
import { AppState, getVideos } from 'src/app/state';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;
  selectedVideo: Video;

  constructor(store: Store<AppState>, ar: ActivatedRoute, router: Router) {
    this.videos = store.select(getVideos).pipe(
      tap(videos => {
        if (!ar.snapshot.queryParams.selectedVideoId && videos && videos.length) {
          // initialize the selected id
          router.navigate(
            [],
            { queryParams: { selectedVideoId: videos[0].id } }
          );
        }
      })
    );
  }
}
