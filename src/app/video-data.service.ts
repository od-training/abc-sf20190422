import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { Video } from './common/interfaces';

const apiUrl = 'https://api.angularbootcamp.com/';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private httpClient: HttpClient) { }

  load() {
    const url = apiUrl + 'videos';
    return this.httpClient.get<Video[]>(url).pipe(
      map(videos => {
        return videos.filter(video => {
          return video.title.startsWith('Angular');
        });
      })
    );
  }

  getVideo(id: string) {
    const url = apiUrl + 'videos/' + id;
    return this.httpClient.get<Video>(url);
  }
}
