import { Action, createFeatureSelector, createSelector } from '@ngrx/store';

import { Video } from './common/interfaces';

const VIDEO_LIST_ARRIVED = 'VIDEO_LIST_ARRIVED';
export class VideoListArrivedAction implements Action {
  type = VIDEO_LIST_ARRIVED;

  constructor(readonly payload: Video[]) { }
}

export interface DashboardState {
  videoList: Video[];
}

export interface AppState {
  dashboard: DashboardState;
}

const initialState: DashboardState = {
  videoList: []
};

export function dashboardReducer(
  state: DashboardState = initialState,
  action: Action
): DashboardState {
  switch (action.type) {
    case VIDEO_LIST_ARRIVED:
      return {
        ...state,
        videoList: (action as VideoListArrivedAction).payload
      };
  }
  return state;
}

const getDashboardState =
  createFeatureSelector<DashboardState>('dashboard');

export const getVideos =
  createSelector(getDashboardState, state => [...state.videoList]);
